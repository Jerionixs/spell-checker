# CMake generated Testfile for 
# Source directory: /home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/src/dict-editor
# Build directory: /home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/realease/dict-editor
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(dict-editor_global_test "/home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/src/../tests/dict-editor/test.sh" "/home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/realease/dict-editor/dict-editor")
SET_TESTS_PROPERTIES(dict-editor_global_test PROPERTIES  WORKING_DIRECTORY "/home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/src/../tests/dict-editor")
