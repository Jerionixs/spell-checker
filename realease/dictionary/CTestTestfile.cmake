# CMake generated Testfile for 
# Source directory: /home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/src/dictionary
# Build directory: /home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/realease/dictionary
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
ADD_TEST(word_list_unit_test "word_list_test")
ADD_TEST(trie_unit_test "trie_test")
ADD_TEST(dictionary_unit_test "dict_test")
