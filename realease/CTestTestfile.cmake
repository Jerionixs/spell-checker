# CMake generated Testfile for 
# Source directory: /home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/src
# Build directory: /home/rolandas/Documents/StudijosWawa/Projekt/test/spellcheck-rmk6380/realease
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(dictionary)
SUBDIRS(dict-editor)
SUBDIRS(dict-check)
