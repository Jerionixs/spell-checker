/** @file
  Prosta implementacja słownika.
  Słownik przechowuje słowa za pomocą struktury danych Trie.

  @ingroup dictionary
  @author Roland Maksimowicz <mak.rolandas@gmail.com>
  @copyright Uniwerstet Warszawski
  @date 2015-05-11
 */

#include "dictionary.h"
#include "trie.h"
#include "conf.h"
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <conf.h>
#include <regex.h>
#include <string.h>

#define _GNU_SOURCE

/**
  Struktura przechowująca słownik.
  Wykorzystuje drzewo Trie
 */
struct dictionary
{
    struct list_trie list;
};

/** @name Funkcje pomocnicze
  @{
 */
/**
  Czyszczenie pamięci słownika
  @param[in,out] dict słownik
 */
void dictionary_free(struct dictionary *dict)
{
    list_trie_done(&dict->list);
}

/**@}*/
/** @name Elementy interfejsu 
  @{
 */
struct dictionary * dictionary_new()
{
    struct dictionary *dict = (struct dictionary *)
            malloc(sizeof(struct dictionary));
    struct list_trie **child = (struct list_trie **)
            malloc(sizeof(struct list_trie*) * CHILD_START);
    struct list_trie *list = &(dict->list);
    list->child = child;
    list_trie_init(&dict->list);
    return dict;
}

void dictionary_done(struct dictionary *dict)
{
    dictionary_free(dict);
    struct list_trie *list = &(dict->list);
    free(list->child);
    free(dict);
}

int dictionary_insert(struct dictionary *dict, const wchar_t *word)
{
    return list_trie_add(&(dict->list), word);
}

int dictionary_delete(struct dictionary *dict, const wchar_t *word)
{
    return list_trie_delete(&(dict->list), word);
}

bool dictionary_find(const struct dictionary *dict, const wchar_t* word)
{
    return dictionary_find_word(&(dict->list), word);
}

void dictionary_hints(const struct dictionary *dict, const wchar_t* word,
                      struct word_list *hintList)
{
    const struct list_trie *list = &(dict->list);
    word_list_init(hintList);
    size_t len = wcslen(word);
    change_letter(list, word, hintList, len);
    add_letter(list, word, hintList, len);
    remove_letter(list, word, hintList, len);
    if (hintList->size != 0)
        quicksort(hintList->array, 0, hintList->size-1);
}

int dictionary_save(const struct dictionary *dict, FILE* stream)
{
    const struct list_trie *list = &(dict->list);
    size_t len = list->childCount;
    size_t i;
    int ret;

    if (len == 0)
        return 0;

    for (i = 0; i < len; i++)
    {
        ret = dictionary_save_recursion(list->child[i], stream);
        if (ret==-1)
            break;
    }
    return ret;
}

struct dictionary * dictionary_load(FILE* stream)
{
    struct dictionary *dict = dictionary_new();
    struct list_trie *list = &(dict->list);
    wchar_t *buf = (wchar_t *) malloc(
                sizeof(wchar_t)*6);
    size_t countRoot = 0;
    while (fscanf(stream, "%32ls", buf) != EOF)
    {
        dictionary_load_recursion(list, stream, buf, countRoot);
        countRoot++;
        if (list->childMax == countRoot)
            reallocate_child(list);
    }
    list->childCount = countRoot;
    list->childMax = CHILD_START;
    if (ferror(stream))
    {
        dictionary_done(dict);
        dict = NULL;
    }
    free(buf);
    return dict;
}

int dictionary_lang_list(char **list, size_t *list_len)
{
    regex_t regex;
    int reti;
    int i = 0;

    char *array = (char *) malloc(
                sizeof(char) * 1000);

    /* Compile regular expression */
    reti = regcomp(&regex, "^.._..", 0);
    if (reti) {
        fprintf(stderr, "Could not compile regex\n");
        free(array);
        regfree(&regex);
        return -2;
    }

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (CONF_PATH)) != NULL) {
      /* print all the files and directories within directory */
      while ((ent = readdir (dir)) != NULL) {
          reti = regexec(&regex, ent->d_name, 0, NULL, 0);
          if (!reti) {
              strncpy(array+i, ent->d_name, strlen(ent->d_name));
              i += strlen(ent->d_name) + 1;
              *(array+i-1) = '\0';
          }

      }
      closedir (dir);
    } else {
      /* could not open directory */
      free(array);
      regfree(&regex);
      return -1;
    }

    *list = array;
    *list_len = i;
    regfree(&regex);
    return 0;
}

struct dictionary * dictionary_load_lang(const char *lang)
{
    struct dictionary *dict;
    int i;

    char *array = (char *) malloc(
                sizeof(char) * 1000);

    strncpy(array, CONF_PATH, strlen(CONF_PATH));
    i = strlen(CONF_PATH);
    *(array+i) = '/';
    i++;
    strncpy(array+i, lang, strlen(lang));
    i += strlen(lang);
    *(array+i) = '\0';

    FILE *f = fopen(array, "r");

    if (!f || !(dict = dictionary_load(f)))
    {
        free(array);
        fprintf(stderr, "Failed to load dictionary\n");
        return NULL;
    }
    else
    {
        fclose(f);
        free(array);
        return dict;
    }
}

int dictionary_save_lang(const struct dictionary *dict, const char *lang)
{
    int i;

    char *filename = (char *) malloc(
                sizeof(char) * 1000);

    strncpy(filename, CONF_PATH, strlen(CONF_PATH));
    i = strlen(CONF_PATH);
    *(filename+i) = '/';
    i++;
    strncpy(filename+i, lang, strlen(lang));
    i += strlen(lang);
    *(filename+i) = '\0';

    FILE *f = fopen(filename, "w");
    if (!f || dictionary_save(dict, f))
    {
        fprintf(stderr, "Failed to save dictionary\n");
        free(filename);
        return -1;
    }
    fclose(f);
    printf("dictionary saved in file %s\n", filename);
    free(filename);
    return 0;
}

/**@}*/

