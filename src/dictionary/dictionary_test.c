/** @file
  Testy na drzewie Trie
  @author Roland Maksimowicz <mak.rolandas@gmail.com>
  @copyright Uniwerstet Warszawski
  @date 2015-05-10
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include "dictionary.h"
#include "trie.h"

const wchar_t* test   = L"a";
const wchar_t* test2   = L"b";
const wchar_t* test3   = L"c";

const wchar_t* word   = L"hęllo";
const wchar_t* word2   = L"hęlk";
const wchar_t* word3   = L"hęlka";
const wchar_t* word4   = L"hęlkoo";
const wchar_t* word5   = L"hęlkooa";

const wchar_t* word_hint   = L"hęlko";

/**
  Inicjalizacja początkowych danych słownika
  @param[out] state Słownik
*/
static int dictionary_setup(void** state) {
    struct dictionary *dict;
    if (!(dict = dictionary_new()))
        return -1;

    dictionary_insert(dict, word);
    dictionary_insert(dict, word2);
    dictionary_insert(dict, word3);
    dictionary_insert(dict, word4);
    dictionary_insert(dict, word5);
    *state = dict;
    return 0;
}

/**
  Test funkcji z komendy hints - change_letter()
  @param[in] state lista dzieci drzewa Trie
*/
static void hints_test(void **state) {
    struct dictionary *dict = *state;
    struct word_list list;

    dictionary_hints(dict, word_hint, &list);
    assert_true(word_list_find(&list, word));
    assert_true(word_list_find(&list, word2));
    assert_true(word_list_find(&list, word3));
    assert_true(word_list_find(&list, word4));
    assert_false(word_list_find(&list, word5));
    assert_int_equal(word_list_size(&list), 4);
    word_list_done(&list);
}

/**
  Zwolnienie zarezerwowanego przes funkcje setup
  @param[in] state Słownik
*/
static int dictionary_teardown(void **state) {
    struct dictionary *dict = *state;
    dictionary_done(dict);
    return 0;
}

/**
  Podstawowa funkcja wywołująca wszystkie testy
*/
int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test_setup_teardown(hints_test, dictionary_setup, dictionary_teardown),

    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
