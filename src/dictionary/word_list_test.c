/** @file
  Testy na liście słów
  @author Roland Maksimowicz <mak.rolandas@gmail.com>
  @copyright Uniwerstet Warszawski
  @date 2015-05-10
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include "word_list.h"

const wchar_t* test   = L"Test string";
const wchar_t* first  = L"First string";
const wchar_t* second = L"Second string";
const wchar_t* third  = L"Third string";

/**
  Test na inicjalizację początkowych wartości
  @param[in] state null
*/
static void word_list_init_test(void** state) {
    struct word_list l;
    word_list_init(&l);
    assert_int_equal(word_list_size(&l), 0);
    word_list_done(&l);
}

/**
  Test na dodawanie słowa
  @param[in] state null
*/
static void word_list_add_test(void** state) {
    struct word_list l;
    word_list_init(&l);
    word_list_add(&l, test);
    assert_int_equal(word_list_size(&l), 1);
    assert_true(wcscmp(test, word_list_get(&l)[0]) == 0);
    word_list_done(&l);
}

/**
  Inicjalizacja listy słów
  @param[out] state Lista słów
*/
static int word_list_setup(void **state) {
    struct word_list *l = malloc(sizeof(struct word_list));
    if (!l) 
        return -1;
    word_list_init(l);
    word_list_add(l, first);
    word_list_add(l, second);
    word_list_add(l, third);
    *state = l;
    return 0;
}

/**
  Inicjalizacja listy słów, test na zwiększenie pojemności
  @param[out] state Lista słów
*/
static int word_list_realloc_setup(void **state) {
    struct word_list *list = malloc(sizeof(struct word_list));
    if (!list)
        return -1;

    wchar_t *buffer = (wchar_t *) malloc(
                sizeof(wchar_t) * 10);
    const wchar_t **array = (const wchar_t **) malloc(
                sizeof(wchar_t *));

    if (!buffer)
        return -1;
    if (!array)
        return -1;

    list->buffer = buffer;
    list->array = array;
    list->size = 0;
    list->buffer_size = 0;
    list->buffer_size_max = 10;
    list->size_max = 1;

    *state = list;
    return 0;
}

/**
  Inicjalizacja listy słów, test na zwiększenie pojemności
  @param[out] state Lista słów
*/
static int word_list_realloc2_setup(void **state) {
    struct word_list *list = malloc(sizeof(struct word_list));
    if (!list)
        return -1;

    wchar_t *buffer = (wchar_t *) malloc(
                sizeof(wchar_t) * 20);
    const wchar_t **array = (const wchar_t **) malloc(
                sizeof(wchar_t *));

    if (!buffer)
        return -1;
    if (!array)
        return -1;

    list->buffer = buffer;
    list->array = array;
    list->size = 0;
    list->buffer_size = 0;
    list->buffer_size_max = 20;
    list->size_max = 1;

    *state = list;
    return 0;
}

/**
  Zwolnienie pamięci po inicjalizacji
  @param[in] state Lista słów
*/
static int word_list_teardown(void **state) {
    struct word_list *l = *state;
    word_list_done(l);
    free(l);
    return 0;
}

/**
  Sprawdzanie zawartości listy
  @param[in] state Lista słów
*/
static void word_list_get_test(void** state) {
    struct word_list *l = *state;
    assert_true(wcscmp(first, word_list_get(l)[0]) == 0);
    assert_true(wcscmp(second, word_list_get(l)[1]) == 0);
    assert_true(wcscmp(third, word_list_get(l)[2]) == 0);
}

/**
  Sprawdzanie funkcji reallocate
  @param[in] state Lista słów
*/
static void word_list_realloc_test(void** state) {
    struct word_list *l = *state;
    word_list_add(l, first);
    word_list_add(l, second);
    word_list_add(l, third);

    assert_true(word_list_find(l, first));
    assert_true(word_list_find(l, second));
    assert_true(word_list_find(l, third));
}

/**
  Sprawdzanie zawartości listy po dodaniu słowa
  @param[in] state Lista słów
*/
static void word_list_repeat_test(void** state) {
    struct word_list *l = *state;
    word_list_add(l, third);
    assert_int_equal(word_list_size(l), 4);
    assert_true(wcscmp(third, word_list_get(l)[3]) == 0);
}

/**
  Podstawowa funkcja wywołująca wszystkie testy
*/
int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(word_list_init_test),
        cmocka_unit_test(word_list_add_test),
        cmocka_unit_test_setup_teardown(word_list_get_test, word_list_setup, word_list_teardown),
        cmocka_unit_test_setup_teardown(word_list_repeat_test, word_list_setup, word_list_teardown),
        cmocka_unit_test_setup_teardown(word_list_realloc_test, word_list_realloc_setup, word_list_teardown),
        cmocka_unit_test_setup_teardown(word_list_realloc_test, word_list_realloc2_setup, word_list_teardown),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
