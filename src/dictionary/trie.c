/** @file
  Implementacja listy słów.

  @ingroup dictionary
  @author Roland Maksimowicz <mak.rolandas@gmail.com>
  @copyright Uniwerstet Warszawski
  @date 2015-05-10
 */

#include "trie.h"
#include <stdlib.h>
#include <stdio.h>


void list_trie_init(struct list_trie *list)
{
    list->childCount = 0;
    list->wordExist = false;
    list->letter = '0';
    list->childMax = CHILD_START;
}

void list_trie_done(struct list_trie *list)
{
    size_t i;

    for(i=0;i<list->childCount;i++)
    {
        list_trie_done(list->child[i]);
    }
    if (list->letter != '0')
    {
        free(list->child);
        free(list);
    }
}

void move_indexes(struct list_trie *list, size_t index)
{
    size_t temp;

    for (temp = list->childCount ;temp>index; temp--)
    {
        list->child[temp]=list->child[temp-1];
    }
}

void reallocate_child(struct list_trie *list)
{
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*) * list->childMax * 2);

    size_t i;
    for (i = 0; i < list->childMax; i++)
    {
        child[i] = list->child[i];
    }
    list->childMax = list->childMax * 2;
    free(list->child);
    list->child = child;
}

size_t search_for_place(const struct list_trie *list, const wchar_t *word, bool *isLetterInList)
{
    size_t begin = 0;
    size_t end = list->childCount;
    size_t middle;

    if (end > 0)
    {
        while(begin < end)
        {
            int temp;
            middle = (begin + end)/2;
            wchar_t temp_letter[2], temp_letter_list[2];
            temp_letter[0]= *word;
            temp_letter[1]= '\0';
            temp_letter_list[0]= list->child[middle]->letter;
            temp_letter_list[1]= '\0';
            temp = wcscoll (temp_letter_list, temp_letter);
            if (temp == 0)
            {
                *isLetterInList = true;
                return middle;
            }

            if (temp > 0)
            {
                end = middle;
                if (end - begin == 0)
                {
                    return begin;
                }
            }
            else
            {
                begin = middle;
                if (end - begin == 1)
                {
                    return begin + 1;
                }
            }

        }
        return begin;
    }
    else
        return 0;
}

void insert_new_word(struct list_trie *list, const wchar_t *word, size_t index) {
    if (*word != '\0') {
        struct list_trie *new_list = (struct list_trie *) malloc(
                sizeof(struct list_trie));
        struct list_trie **child = (struct list_trie **) malloc(
                sizeof(struct list_trie*) * CHILD_START);

        new_list->child = child;

        list->child[index] = new_list;
        new_list->childCount = 0;
        new_list->childMax = CHILD_START;
        new_list->wordExist = false;
        new_list->letter = *word;
        word++;
        list->childCount++;

        insert_new_word(new_list, word, 0);

        if (*word == '\0')
            new_list->wordExist = true;
    }
}

int list_trie_add(struct list_trie *list, const wchar_t *word)
{
    size_t index;
    struct list_trie *temp_list = list;
    bool isLetterInList = false;
    size_t len = wcslen(word);
    const wchar_t *temp_word = word;
    while(len > 0)
    {
        if (temp_list->childCount == temp_list->childMax)
            reallocate_child(temp_list);

        index = search_for_place(temp_list, temp_word, &isLetterInList);
        if (!isLetterInList)
        {
            move_indexes(temp_list, index);
            insert_new_word(temp_list, temp_word, index);
            return 1;
        }

        isLetterInList = false;
        temp_word++;

        temp_list = temp_list->child[index];
        len--;
    }
    if (temp_list->wordExist)
        return 0;
    else
    {
        temp_list->wordExist = true;
        return 1;
    }
}

void move_indexes_on_delete(struct list_trie *list, size_t index)
{
    size_t temp = list->childCount;
    size_t i;

    for (i = index ;i < temp; i++)
    {
        list->child[i]=list->child[i+1];
    }
}


int list_trie_delete_recursion(struct list_trie *list_before, struct list_trie *list, const wchar_t *word, size_t indexPrev) {

    size_t index;
    bool isLetterInList = false;

    index = search_for_place(list, word, &isLetterInList);

    if (isLetterInList || *word == '\0')
    {
        if (*word != '\0')
        {
            int temp;
            word++;
            temp = list_trie_delete_recursion(list, list->child[index], word, index);

            if (temp == 1)
            {
                if (list->childCount == 0 && !list->wordExist)
                {
                    free(list->child);
                    free(list);
                    list_before->child[indexPrev] = NULL;
                    list_before->childCount--;
                    move_indexes_on_delete(list_before, indexPrev);
                }
            }
            return temp;
        }
        else
        {
            if (!list->wordExist)
                return 0;
            if (list->childCount == 0)
            {
                free(list->child);
                free(list);
                list_before->child[indexPrev] = NULL;
                list_before->childCount--;
                move_indexes_on_delete(list_before, indexPrev);
            }
            else
            {
                list->wordExist=false;
            }
            return 1;
        }
    }
    else
        return 0;
}

int list_trie_delete(struct list_trie *list, const wchar_t *word) {

    size_t index;
    bool isLetterInList = false;

    index = search_for_place(list, word, &isLetterInList);
    word++;
    if (isLetterInList)
        return (list_trie_delete_recursion(list, list->child[index], word, index));

    return 0;
}

bool dictionary_find_word(const struct list_trie *list, const wchar_t* word)
{
    size_t i, index;
    size_t len = wcslen(word);
    bool isLetterInList = false;
    for (i = 0; i < len; i++)
    {
        index = search_for_place(list, word, &isLetterInList);
        if (!isLetterInList)
            break;

        isLetterInList = false;
        list = list->child[index];
        word++;
    }
    if (i==len)
    {
        if (list->wordExist)
            return true;
    }
    return false;
}

void move_index_back(wchar_t* word, size_t index, size_t len)
{
    size_t temp;

    for (temp = index; temp < len-1; temp++) {
        *(word+temp) = *(word + temp + 1);
    }
    *(word+len-1) = L'\0';
}

void move_index(wchar_t* word, size_t index, size_t len)
{
    size_t temp;

    for (temp = len+1; temp > index; temp--) {
        *(word+temp) = *(word + temp - 1);
    }
    *(word+len+1) = L'\0';
}

void remove_letter(const struct list_trie *list, const wchar_t* word, struct word_list *hintList, size_t len)
{
    //MAX_WORD_LENGTH+2
    wchar_t *tempWord =
            (wchar_t *) malloc(sizeof(wchar_t) * len+4);

    tempWord = wcscpy (tempWord, word);
    size_t i;

    wchar_t tempLetter = *tempWord;
    move_index_back(tempWord, 0, len);
    for (i = 0; i < len + 1; i++) {
            if (dictionary_find_word(list, tempWord))
                if (!word_list_find(hintList, tempWord))
                    word_list_add(hintList, tempWord);

        wchar_t tempLetterSave = *(tempWord + i);
        if (tempLetterSave == '\0')
            break;
        *(tempWord + i) = tempLetter;
        tempLetter = tempLetterSave;

    }
    free(tempWord);
}

void add_letter(const struct list_trie *list, const wchar_t* word, struct word_list *hintList, size_t len)
{
    //MAX_WORD_LENGTH+2
    wchar_t *tempWord =
            (wchar_t *) malloc(sizeof(wchar_t) * 65);

    tempWord = wcscpy (tempWord, word);

    const struct list_trie *rootList = list;

    size_t i, j;
    size_t index;
    bool isLetterInList = false;

    move_index(tempWord, 0, len);

    for (i = 0; i < len + 1; i++) {
        for (j = 0; j < list->childCount; j++) {
            *(tempWord + i) = list->child[j]->letter;
            if (dictionary_find_word(rootList, tempWord))
                if (!word_list_find(hintList, tempWord))
                    word_list_add(hintList, tempWord);
        }
        *(tempWord + i) = *(tempWord + i + 1);
        isLetterInList = false;
        index = search_for_place(list, word + i, &isLetterInList);
        if (!isLetterInList)
            break;
        list = list->child[index];
    }
    free(tempWord);
}

void change_letter(const struct list_trie *list, const wchar_t* word, struct word_list *hintList, size_t len)
{
    size_t i, j;

    wchar_t *tempWord =
                (wchar_t *) malloc(sizeof(wchar_t) * 65);

        tempWord = wcsncpy (tempWord, word, len+1);

    const struct list_trie *rootList = list;

    size_t index;
    bool isLetterInList = false;

    if (dictionary_find_word(list, word))
        if (!word_list_find(hintList, tempWord))
            word_list_add(hintList, word);

    for(i = 0; i < len; i++)
    {
        for (j = 0; j < list->childCount; j++) {
            wchar_t tempLetter = *(tempWord+i);
            if (*(tempWord+i) != list->child[j]->letter)
            {
                *(tempWord+i) = list->child[j]->letter;
                if (dictionary_find_word(rootList, tempWord))
                    if (!word_list_find(hintList, tempWord))
                        word_list_add(hintList, tempWord);
            }
            *(tempWord+i) = tempLetter;
        }
        isLetterInList = false;
        index = search_for_place(list, word+i, &isLetterInList);
        if (!isLetterInList)
            break;
        list = list->child[index];
    }
    free(tempWord);
}

size_t partition(const wchar_t **array, size_t p, size_t r)
{
    const wchar_t *x = array[p];
    const wchar_t *w;
    size_t i = p, j = r;
    while (true)
    {
        while (wcscoll(array[j], x) > 0)
            j--;
        while (wcscoll(array[i], x) < 0)
            i++;
        if (i < j)
                {
            w = array[i];
            array[i] = array[j];
            array[j] = w;
            i++;
            j--;
        } else
            return j;
    }
}

void quicksort(const wchar_t **array, size_t p, size_t r)
{
    size_t q;
    if (p < r) {
        q = partition(array, p, r);
        quicksort(array, p, q);
        quicksort(array, q + 1, r);
    }
}

int dictionary_save_recursion(const struct list_trie *list, FILE* stream) {
    size_t len = list->childCount;
    size_t i;
    int ret = 0;

    if (fprintf(stream, "%lc", list->letter) < 0)
        return -1;

    if (list->wordExist)
    {
        if (fprintf(stream, "%c", 't') < 0)
                return -1;
    }
    else
    {
        if (fprintf(stream, "%c", 'f') < 0)
                return -1;
    }

    if (fprintf(stream, "%zd\n", list->childCount) < 0)
        return -1;

    for (i = 0; i < len; i++) {
        ret = dictionary_save_recursion(list->child[i], stream);
    }
    return ret;
}

void dictionary_load_recursion(struct list_trie *list, FILE* stream, wchar_t *buf, size_t index)
{
    struct list_trie *new_list = (struct list_trie *) malloc(
                    sizeof(struct list_trie));

    struct list_trie **child = (struct list_trie **) malloc(
                        sizeof(struct list_trie*) * CHILD_START);

    new_list->child = child;

    new_list->letter = *buf;
    buf++;
    if (*buf == 't')
        new_list->wordExist = true;
    else
        if(*buf == 'f')
            new_list->wordExist = false;
        else
        {
            free(new_list);
            free(child);
            return;
        }
    buf++;

    int childCount = wcstol(buf,NULL,0);
    int i;

    new_list->childCount = childCount;
    new_list->childMax = CHILD_START;
    while (list->childMax <= childCount)
        reallocate_child(list);

    buf--;
    buf--;
    list->child[index]= new_list;
    for (i = 0; i < childCount; i++)
    {
        if (fscanf(stream, "%32ls", buf) < 0)
            break;
        dictionary_load_recursion(new_list, stream, buf, i);
    }
}
