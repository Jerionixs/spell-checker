/** @file
    Interfejs drzewa Trie.
    @ingroup dictionary
    @author Roland Maksimowicz <mak.rolandas@gmail.com>
    @copyright Uniwerstet Warszawski
    @date 2015-05-29
 */


#include <wchar.h>
#include <stdbool.h>
#include <word_list.h>

/**
  Liczba dzieci implementowana na początek
  Jeśli ta liczba zostanie przekroczona,
  zostaje ona powiększona
  */
#define CHILD_START 32

/**
  Struktura Trie nieskompresowana.
  */

struct list_trie
{
    /// Znak przechowywany w więźle
    wchar_t letter;
    /// Czy dane słowo egzystuje
    bool wordExist;
    /// Liczba dzieci, które posiada dany węzeł
    size_t childCount;
    /// Maksymalna liczba przechowywanych dzieci
    /// (Jeśli zostanie limit przekroczony,
    /// zostanie zaimplementowana nowa tablica)
    size_t childMax;
    /// Wskaźnik do tablicy, która przechowuje dzieci
    struct list_trie **child;
};

/**
  Inicjuje korzeń drzewa Trie.
  @param[in,out] list Lista dzieći korzenia drzewa Trie.
  */
void list_trie_init(struct list_trie *list);

/**
  Destrukcja drzewa Trie.
  @param[in,out] list Lista słów.
  */
void list_trie_done(struct list_trie *list);

/**
  Dodaje słowo do drzewa trie.
  @param[in,out] list Lista dzieci korzenia drzewa Trie.
  @param[in] word Dodawane słowo.
  @return 1 jeśli się udało, 0 w p.p.
  */
int list_trie_add(struct list_trie *list, const wchar_t *word);

/**
  Usuwa słowo z drzewa trie.
  @param[in,out] list Lista dzieci korzenia drzewa Trie.
  @param[in] word Usuwane słowo.
  @return 1 jeśli się udało, 0 w p.p.
  */
int list_trie_delete(struct list_trie *list, const wchar_t *word);

/**
  Znajduje index dziecka w liście,
  bądź miejsce dziecka po którym powinno się znaleźć nowe dziecko.
  @param[in] list Lista dzieci węzła.
  @param[in] index Index od którego zaczyna przesuwanie dzieci w prawo.
  @param[in, out] isLetterInList Jeśli dane dziecko znajduje się w liście zwraca true
  @return index dziecka jeśli słowo znajduje się w liście,
    w p.p. index dziecka po którym dane słowo powinno się znajdować
*/
size_t search_for_place(const struct list_trie *list, const wchar_t *word, bool *isLetterInList);

/**
  Zwiększa pojemność listy dzieci dwukrotnie.
  @param[in,out] list Lista dzieci węzła.
  @param[in] index Index od którego zaczyna przesuwanie dzieci w prawo.
  */
void reallocate_child(struct list_trie *list);

/**
  Przesuwa dzieci w liście o jedno pole.
  @param[in,out] list Lista dzieci korzenia drzewa Trie.
  @param[in] index Index od którego zaczyna przesuwanie dzieci w prawo.
  */
void move_indexes(struct list_trie *list, size_t index);

/**
  Tworzy nowe węzły w których umieszcza nowe słowo
  @param[in, out] list Lista dzieci węzła.
  @param[in] word Dodane słowo
  @param[in] index Miejsce, w którym dodane zostanie nowe słowo
*/
void insert_new_word(struct list_trie *list, const wchar_t *word, size_t index);

/**
  Przesuwa dzieci w liście o jedno pole w lewo.
  @param[in,out] list Lista dzieci korzenia drzewa Trie.
  @param[in] index Index od którego zaczyna przesuwanie dzieci w lewo.
  */
void move_indexes_on_delete(struct list_trie *list, size_t index);

/**
  Usuwa słowo za pomocą rekurencyjnego wywołania z drzewa trie.
  @param[in,out] list_before Lista dzieci drzewa Trie.
  @param[in,out] list Lista dzieci drzewa Trie.
  @param[in] word Usuwane słowo.
  @param[in] indexPrev Adres węzła "list" w liście "list_before".
  @return 1 jeśli się udało, 0 w p.p.
  */
int list_trie_delete_recursion(struct list_trie *list_before, struct list_trie *list, const wchar_t *word, size_t indexPrev);

/**
  Sprawdza, czy dane słowo znajduje się w słowniku.
  @param[in] list Lista dzieci korzenia drzewa Trie.
  @param[in] word Szukane słowo.
  @return Wartość logiczna czy `word` jest w słowniku.
  */
bool dictionary_find_word(const struct list_trie *list, const wchar_t* word);

/**
  Sortuje liste słów wg. alfabetu (Sortowanie szybkie)
  @param[in, out] array tablica wskaźników do listy słów
  @param[in] początek tablicy
  @param[in] indeks ostatniego słowa w tablicy
  */
void quicksort(const wchar_t **array, size_t p, size_t r);

/**
  Dzielimy tablice na dwie części, w pierwszej wszystkie
  liczby są mniejsze bądź równe x, w drugiej większe lub równe od x
  @param[in, out] array tablica wskaźników do listy słów
  @param[in] początek tablicy
  @param[in] indeks ostatniego słowa w tablicy
  @return zwracamy indeks jako punkt podziału tablicy
  */
size_t partition(const wchar_t **array, size_t p, size_t r);

/**
  Dodaje słowo do listy, jeśli nie zgadza się tylko jedna litera,
  Długość słowo jest taka sama
  @param[in] dict słownik.
  @param[in] word Słowo, dla którego szukamy podpowiedzi.
  @param[in, out] hintList Lista słów, w której są przetrzymywane wszystkie
  podpowiedzi.
  @param[in] len długość słowa word.
  */
void change_letter(const struct list_trie *list, const wchar_t* word, struct word_list *hintList, size_t len);

/**
  Dodaje słowo do listy, jeśli zawiera o jedną literę więcej.
  @param[in] dict słownik.
  @param[in] word Słowo, dla którego szukamy podpowiedzi.
  @param[in, out] hintList Lista słów, w której są przetrzymywane wszystkie
  podpowiedzi.
  @param[in] len długość słowa word.
  */
void add_letter(const struct list_trie *list, const wchar_t* word, struct word_list *hintList, size_t len);

/**
  Dodaje słowo do listy, jeśli zawiera o jedną literę mniej.
  @param[in] dict słownik.
  @param[in] word Słowo, dla którego szukamy podpowiedzi.
  @param[in, out] hintList Lista słów, w której są przetrzymywane wszystkie
  podpowiedzi.
  @param[in] len długość słowa word.
  */
void remove_letter(const struct list_trie *list, const wchar_t* word, struct word_list *hintList, size_t len);

/**
  Zwiększa długość słowa o jeden znak przesuwając wszystkie znaki w prawo zaczynając
  od podanego indeksu.
  @param[in, out] word Słowo, dla którego szukamy podpowiedzi.
  @param[in] index miejsce od którego zostają przesunięte wszystkie znaki w prawo.
  @param[in] len długość słowa word.
  */
void move_index(wchar_t* word, size_t index, size_t len);

/**
  Zmniejsza długość słowa o jeden znak przesuwając wszystkie znaki w lewo zaczynając
  od podanego indeksu.
  @param[in, out] word Słowo, dla którego szukamy podpowiedzi.
  @param[in] index miejsce od którego zostają przesunięte wszystkie znaki w prawo.
  @param[in] len długość słowa word.
  */
void move_index_back(wchar_t* word, size_t index, size_t len);

/**
  Wczytuje słownik
  @param[in,out] list Lista dzieci drzewa Trie.
  @param[in,out] stream Strumień, gdzie ma być zapisany słownik.
  @param[in,out] buf Buffer, który wczytuje dane z pliku
  @param[in] index Indeks tablicy, gdzie zostanie zapisany nowy węzęł.
  */
void dictionary_load_recursion(struct list_trie *list, FILE* stream, wchar_t *buf, size_t index);

/**
  Zapisuje słownik.
  Kod zapisu: Jedna linijka zawiera 3 znaki.
  1 - oznacza literę danego węzła,
  2 - jeśli słowo istniej (t), w p.p. (f),
  3 - liczba dzieci danego węzła
  @param[in] list Lista dzieci drzewa Trie.
  @param[in,out] stream Strumień, gdzie ma być zapisany słownik.
  @return <0 jeśli operacja się nie powiedzie, 0 w p.p.
  */
int dictionary_save_recursion(const struct list_trie *list, FILE* stream);
