/** @file
    Interfejs listy słów.

    @ingroup dictionary
    @author Roland Maksimowicz <mak.rolandas@gmail.com>
    @copyright Uniwerstet Warszawski
    @date 2015-05-10
 */

#ifndef __WORD_LIST_H__
#define __WORD_LIST_H__

#include <wchar.h>
#include <stdbool.h>

/**
  Maksymalna liczba słów przechowywana w liście słów.
  Jeśli wartość zostanie przekroczona, lista powiększy się
  dwukrotnie
  */
#define WORD_LIST_MAX_WORDS 32


/**
  Łączna liczba znaków słów przechowywanych w liście słów
  włącznie ze znakami \0 kończącymi słowo.
  Jeśli wartość zostanie przekroczona, lista powiększy się
  dwukrotnie.
  */
#define WORD_LIST_SUM 1024

/**
  Struktura przechowująca listę słów.
  Należy używać funkcji operujących na strukturze,
  gdyż jej implementacja może się zmienić.
  */
struct word_list
{

    /// Liczba słów.
    size_t size;
    /// Łączna liczba znaków.
    size_t buffer_size;
    /// Tablica słów.
    /// Liczba słów.
    size_t size_max;
    /// Łączna liczba znaków.
    size_t buffer_size_max;
    /// Tablica słów.
    const wchar_t **array;
    /// Bufor, w którym pamiętane są słowa.
    wchar_t *buffer;
};

/**
  Inicjuje listę słów.
  @param[in,out] list Lista słów.
  */
void word_list_init(struct word_list *list);

/**
  Destrukcja listy słów.
  @param[in,out] list Lista słów.
  */
void word_list_done(struct word_list *list);

/**
  Dodaje słowo do listy.
  @param[in,out] list Lista słów.
  @param[in] word Dodawane słowo.
  @return 1 jeśli się udało, 0 w p.p.
  */
int word_list_add(struct word_list *list, const wchar_t *word);

/**
  Sprawdza, czy dane słowo snajduje się w liście słów.
  @param[in] list lista dzieci drzewa Trie.
  @param[in] word Słowo, którego szukamy w liście słów.
  @return true, jeśli słowo znajduje się w liście w p.p. false
  */
bool word_list_find(const struct word_list *list, const wchar_t* word);

/**
  Zwiększa pojemność listy słów dwukrotnie
  @param[in, out] list Lista słów.
  */
void reallocate_word_list(struct word_list *list);

/**
  Zwraca liczę słów w liście.
  @param[in] list Lista słów.
  @return Liczba słów w liście.
  */
static inline
size_t word_list_size(const struct word_list *list)
{
    return list->size;
}

/**
  Zwraca tablicę słów w liście.
  @param[in] list Lista słów.
  @return Tablica słów.
  */
static inline
const wchar_t * const * word_list_get(const struct word_list *list)
{
    return list->array;
}

#endif /* __WORD_LIST_H__ */
