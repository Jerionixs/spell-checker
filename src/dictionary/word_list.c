/** @file
  Implementacja listy słów.

  @ingroup dictionary
  @author Jakub Pawlewicz <pan@mimuw.edu.pl>
  @copyright Uniwerstet Warszawski
  @date 2015-05-10
 */

#include "word_list.h"
#include <stdlib.h>

/** @name Elementy interfejsu 
   @{
 */

void word_list_init(struct word_list *list)
{
    wchar_t *buffer = (wchar_t *) malloc(
                sizeof(wchar_t) * WORD_LIST_SUM);
    const wchar_t **array = (const wchar_t **) malloc(
                sizeof(wchar_t *) * WORD_LIST_MAX_WORDS);

    list->buffer = buffer;
    list->array = array;
    list->size = 0;
    list->buffer_size = 0;
    list->buffer_size_max = WORD_LIST_SUM;
    list->size_max = WORD_LIST_MAX_WORDS;
}

void word_list_done(struct word_list *list)
{
    free(list->array);
    free(list->buffer);
}

void reallocate_word_list(struct word_list *list)
{
    wchar_t *buffer = (wchar_t *) malloc(
            sizeof(wchar_t) * list->buffer_size_max * 2);
    const wchar_t **array = (const wchar_t **) malloc(
            sizeof(wchar_t *) * list->size_max * 2);

    size_t i, j = 0;
    for (i = 0; i < list->buffer_size; i++)
    {
        buffer[i] = list->buffer[i];
        if (j < list->size) {
            if (&(list->buffer[i]) == list->array[j]) {
                array[j] = &(buffer[i]);
                j++;
            }
        }
    }

    list->size_max = list->size_max * 2;
     list->buffer_size_max = list->buffer_size_max * 2;
    free(list->buffer);
    free(list->array);
    list->buffer = buffer;
    list->array = array;
}

int word_list_add(struct word_list *list, const wchar_t *word)
{
    if (list->size >= list->size_max)
        reallocate_word_list(list);
    size_t len = wcslen(word) + 1;
    if (list->buffer_size + len > list->buffer_size_max)
        reallocate_word_list(list);
    wchar_t *pos = list->buffer + list->buffer_size;
    list->array[list->size++] = pos;
    wcscpy(pos, word);
    list->buffer_size += len;
    return 1;
}

bool word_list_find(const struct word_list *list, const wchar_t* word)
{
    const wchar_t * const * a = word_list_get(list);
    for (size_t i = 0; i < word_list_size(list); i++)
        if (!wcscmp(a[i], word))
            return true;
    return false;
}

/**@}*/
