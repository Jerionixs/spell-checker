/** @file
  Testy na drzewie Trie
  @author Roland Maksimowicz <mak.rolandas@gmail.com>
  @copyright Uniwerstet Warszawski
  @date 2015-05-10
 */

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <cmocka.h>
#include "trie.h"
#include "word_list.h"
#include <stdio.h>

const wchar_t* test   = L"a";
const wchar_t* test2   = L"b";
const wchar_t* test3   = L"c";
const wchar_t* test4   = L"h";

const wchar_t* word   = L"hęllo";
const wchar_t* word2   = L"hęl";
const wchar_t* word3   = L"hęlka";
const wchar_t* word4   = L"hęllooo";
const wchar_t* word5   = L"hęllooortdfg";

const wchar_t* word_reallock = L"aę";
const wchar_t* word_reallock2 = L"hę";

const wchar_t* remove_hint_word   = L"bhęllo";
const wchar_t* remove_hint_word2   = L"hęlb";
const wchar_t* remove_hint_word3   = L"hęllka";
const wchar_t* add_hint_word   = L"hęll";
const wchar_t* add_hint_word2   = L"hl";
const wchar_t* add_hint_word3   = L"ęlka";
const wchar_t* change_hint_word   = L"hęlla";
const wchar_t* change_hint_word2   = L"hal";
const wchar_t* change_hint_word3   = L"ęęlka";

/**
  Test na inicjalizację początkowych wartości
*/
static void trie_init_test(void** state) {
    struct list_trie l;
    list_trie_init(&l);
    assert_int_equal((&l)->childMax, CHILD_START);
    assert_int_equal((&l)->childCount, 0);
    assert_false((&l)->wordExist);
    assert_int_equal((&l)->letter, '0');
    list_trie_done(&l);
}

/**
  Test funkcji pomocniczej move_indexes, przesuwa wszystkie indeksy
  w prawo zaczynając od podanego indexu
  @param[in] state null
*/
static void trie_move_index_test(void** state) {

    struct list_trie *l = *state;
    struct list_trie *l2 = l->child[0];
    move_indexes(l, 0);
    assert_int_equal(l->child[1], l2);
}


/**
  Test funkcji pomocniczej move_indexes_on_delete, przesuwa wszystkie indeksy
  w lewo zaczynając od podanego indexu
  @param[in] state null
*/
static void trie_move_index_on_delete_test(void** state) {

    struct list_trie *l = *state;
    struct list_trie *l2 = l->child[1];
    move_indexes_on_delete(l, 0);
    assert_int_equal(l->child[0], l2);
}
/**
  Inicjalizacja początkowych danych dla testu trie_rallocate_child_test
  @param[out] state lista dzieci drzewa Trie
*/
static int list_trie_reallocate_setup(void **state) {
    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*));

    struct list_trie *list2 = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child2 = (struct list_trie **) malloc(
                    sizeof(struct list_trie*));
    if (!child)
        return -1;
    if (!list)
        return -1;
    list_trie_init(list);
    list_trie_init(list2);
    list->childMax = 1;
    list->child = child;
    list->child[0] = list2;
    list->childCount = 1;
    list2->child = child2;
    list2->childMax = 1;
    list2->letter = 'a';
    *state = list;
    return 0;
}

/**
  Inicjalizacja początkowych danych dla testu search_for_place_test
  @param[out] state lista dzieci drzewa Trie
*/
static int list_trie_delete_index_setup(void **state) {
    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*)*CHILD_START);

    struct list_trie *list2 = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child2 = (struct list_trie **) malloc(
                    sizeof(struct list_trie*)*CHILD_START);
    if (!child)
        return -1;
    if (!list)
        return -1;
    list_trie_init(list);
    list_trie_init(list2);
    list->child = child;
    list->child[1] = list2;
    list->childCount++;
    list2->child = child2;
    list2->letter = 'b';
    *state = list;
    return 0;
}

/**
  Inicjalizacja początkowych danych dla testu search_for_place_test
  @param[out] state lista dzieci drzewa Trie
*/
static int list_trie_delete_word_setup(void **state) {
    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*)*CHILD_START);

    if (!child)
        return -1;
    if (!list)
        return -1;
    list_trie_init(list);
    list->child = child;
    list_trie_add(list, word);
    list_trie_add(list, word2);
    list_trie_add(list, word3);
    list_trie_add(list, word4);

    *state = list;
    return 0;
}

/**
  Inicjalizacja początkowych danych
  @param[out] state lista dzieci drzewa Trie
*/
static int list_trie_setup(void **state) {
    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*)*CHILD_START);

    struct list_trie *list2 = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child2 = (struct list_trie **) malloc(
                    sizeof(struct list_trie*)*CHILD_START);
    if (!child)
        return -1;
    if (!list)
        return -1;
    list_trie_init(list);
    list_trie_init(list2);
    list->child = child;
    list->child[0] = list2;
    list->childCount++;
    list2->child = child2;
    list2->letter = 'b';
    *state = list;
    return 0;
}


/**
  Inicjalizacja początkowych danych dla testu insert_new_word_test
  @param[out] state lista dzieci drzewa Trie
*/
static int list_trie_add_setup(void **state) {
    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*)*CHILD_START);

    if (!child)
        return -1;
    if (!list)
        return -1;
    list_trie_init(list);
    list->child = child;
    *state = list;
    return 0;
}

/**
  Inicjalizacja początkowych danych dla testu insert_new_word3_test
  Możliwość przetestowania funkcji list_trie_add() z reallocate_child()
  @param[out] state lista dzieci drzewa Trie
*/
static int list_trie_add2_setup(void **state) {
    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*));

    if (!child)
        return -1;
    if (!list)
        return -1;
    list_trie_init(list);
    list->childMax = 1;
    list->child = child;
    *state = list;
    return 0;
}
/**
  Test na powiększenie pojemności tablicy dzieci drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void trie_reallocate_child_test(void **state) {
    struct list_trie *l = *state;
    reallocate_child(l);
    assert_int_equal(l->childMax, 2);
    assert_int_equal(l->child[0]->childMax, 1);
    assert_int_equal(l->child[0]->letter, 'a');
}

/**
  Test na powiększenie pojemności tablicy dzieci drzewa Trie
  Sprawdzanie, czy pamięć została powiększona
  @param[in] state lista dzieci drzewa Trie
*/
static void trie_reallocate_child2_test(void **state) {
    struct list_trie *l = *state;

    reallocate_child(l);

    struct list_trie *list = (struct list_trie *) malloc(
            sizeof(struct list_trie));
    struct list_trie **child = (struct list_trie **) malloc(
                    sizeof(struct list_trie*));

    list_trie_init(list);
    list->childMax = 1;
    l->child[1] = list;
    list->child = child;
    l->childCount++;
    l->child[1]->letter = 'b';
    assert_int_equal(l->child[1]->letter, 'b');
}

/**
  Test na znalezienie indeksu dla nowego słowa w tablicy dzieci drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void trie_search_for_place_test(void **state) {
    struct list_trie *l = *state;

    bool isLetterInList = false;
    size_t index;

    index = search_for_place(l, test, &isLetterInList);
    assert_int_equal(index, 0);
    assert_false(isLetterInList);
}

/**
  Test na znalezienie indeksu dla nowego słowa w tablicy dzieci drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void trie_search_for_place2_test(void **state) {
    struct list_trie *l = *state;

    bool isLetterInList = false;
    size_t index;

    index = search_for_place(l, test2, &isLetterInList);
    assert_int_equal(index, 0);
    assert_true(isLetterInList);
}

/**
  Test na znalezienie indeksu dla nowego słowa w tablicy dzieci drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void trie_search_for_place3_test(void **state) {
    struct list_trie *l = *state;

    bool isLetterInList = false;
    size_t index;

    index = search_for_place(l, test3, &isLetterInList);
    assert_int_equal(index, 1);
    assert_false(isLetterInList);
}

/**
  Test na znalezienie indeksu dla nowego słowa w tablicy dzieci drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void trie_search_for_place4_test(void **state) {
    struct list_trie *l = *state;

    bool isLetterInList = false;
    size_t index;

    index = search_for_place(l, test3, &isLetterInList);
    assert_int_equal(index, 0);
    assert_false(isLetterInList);
}

/**
  Test na szukanie słowa
  @param[in] state lista dzieci drzewa Trie
*/
static void find_word_test(void **state) {
    struct list_trie *list = *state;

    assert_true(dictionary_find_word(list, word));
    assert_true(dictionary_find_word(list, word2));
    assert_true(dictionary_find_word(list, word3));
    assert_true(dictionary_find_word(list, word4));
    assert_false(dictionary_find_word(list, word_reallock));
    assert_false(dictionary_find_word(list, word_reallock2));
    assert_false(dictionary_find_word(list, word5));
}

/**
  Test na dodanie nowego słowa do drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void insert_new_word_test(void **state) {
    struct list_trie *l = *state;

    insert_new_word(l, word, 0);
    assert_true(dictionary_find_word(l, word));
    assert_false(dictionary_find_word(l, word2));
    assert_false(dictionary_find_word(l, word4));
}
/**
  Test na dodanie nowego słowa do drzewa Trie
  @param[in] state lista dzieci drzewa Trie
*/
static void insert_new_word2_test(void **state) {
    struct list_trie *l = *state;

    list_trie_add(l, word);
    list_trie_add(l, word2);
    list_trie_add(l, word3);
    list_trie_add(l, word4);
    list_trie_add(l, word4);
    assert_true(dictionary_find_word(l, word));
    assert_true(dictionary_find_word(l, word2));
    assert_true(dictionary_find_word(l, word3));
    assert_true(dictionary_find_word(l, word4));
    assert_false(dictionary_find_word(l, word_reallock));
    assert_false(dictionary_find_word(l, word_reallock2));
    assert_false(dictionary_find_word(l, word5));
}

/**
  Test na dodanie nowego słowa do drzewa Trie
  z przekroczeniem limitu
  @param[in] state lista dzieci drzewa Trie
*/
static void insert_new_word3_test(void **state) {
    struct list_trie *l = *state;

    list_trie_add(l, word_reallock);
    list_trie_add(l, word_reallock2);
    assert_false(dictionary_find_word(l, word2));
    assert_true(dictionary_find_word(l, word_reallock));
    assert_true(dictionary_find_word(l, word_reallock2));
    assert_false(dictionary_find_word(l, word5));
    assert_false(dictionary_find_word(l, test4));

}

/**
  Test funkcji list_trie_delete_recursion
  @param[in] state lista dzieci drzewa Trie
*/
static void delete_recursion_test(void **state) {
    struct list_trie *l = *state;

    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word+1, 0), 1);
    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word+1, 0), 0);
    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word4+1, 0), 1);
    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word4+1, 0), 0);
    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word3+1, 0), 1);
    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word3+1, 0), 0);
    assert_int_equal(list_trie_delete_recursion(l, l->child[0], word2+1, 0), 1);
}

/**
  Test funkcji list_trie_delete
  @param[in] state lista dzieci drzewa Trie
*/
static void delete_test(void **state) {
    struct list_trie *l = *state;

    assert_int_equal(list_trie_delete(l, word2), 1);
    assert_int_equal(list_trie_delete(l, word2), 0);
    assert_int_equal(list_trie_delete(l, word), 1);
    assert_int_equal(list_trie_delete(l, word), 0);
    assert_int_equal(list_trie_delete(l, word4), 1);
    assert_int_equal(list_trie_delete(l, word4), 0);
    assert_int_equal(list_trie_delete(l, word3), 1);
    assert_int_equal(list_trie_delete(l, word3), 0);

}

/**
  Test funkcji z komendy hints - remove_letter()
  @param[in] state lista dzieci drzewa Trie
*/
static void hints_remove_test(void **state) {
    struct list_trie *tL = *state;

    struct word_list *wL = malloc(sizeof(struct word_list));

    if (!wL)
        assert_true(false);
    word_list_init(wL);

    remove_letter(tL, remove_hint_word, wL, 6);
    remove_letter(tL, remove_hint_word2, wL, 4);
    remove_letter(tL, remove_hint_word3, wL, 6);
    assert_true(word_list_find(wL, word));
    assert_true(word_list_find(wL, word2));
    assert_true(word_list_find(wL, word3));
    assert_false(word_list_find(wL, word4));
    assert_false(word_list_find(wL, remove_hint_word));
    assert_int_equal(word_list_size(wL), 3);

    word_list_done(wL);
    free(wL);
}

/**
  Test funkcji z komendy hints - add_letter()
  @param[in] state lista dzieci drzewa Trie
*/
static void hints_add_test(void **state) {
    struct list_trie *tL = *state;

    struct word_list *wL = malloc(sizeof(struct word_list));

    if (!wL)
        assert_true(false);
    word_list_init(wL);

    add_letter(tL, add_hint_word, wL, 4);
    add_letter(tL, add_hint_word2, wL, 2);
    add_letter(tL, add_hint_word3, wL, 4);
    assert_true(word_list_find(wL, word));
    assert_true(word_list_find(wL, word2));
    assert_true(word_list_find(wL, word3));
    assert_false(word_list_find(wL, word4));
    assert_false(word_list_find(wL, add_hint_word));
    assert_int_equal(word_list_size(wL), 3);

    word_list_done(wL);
    free(wL);
}

/**
  Test funkcji z komendy hints - change_letter()
  @param[in] state lista dzieci drzewa Trie
*/
static void hints_change_test(void **state) {
    struct list_trie *tL = *state;

    struct word_list *wL = malloc(sizeof(struct word_list));

    if (!wL)
        assert_true(false);
    word_list_init(wL);

    change_letter(tL, change_hint_word, wL, 5);
    change_letter(tL, change_hint_word2, wL, 3);
    change_letter(tL, change_hint_word3, wL, 5);
    assert_true(word_list_find(wL, word));
    assert_true(word_list_find(wL, word2));
    assert_true(word_list_find(wL, word3));
    assert_false(word_list_find(wL, word4));
    assert_false(word_list_find(wL, change_hint_word));
    assert_int_equal(word_list_size(wL), 3);

    word_list_done(wL);
    free(wL);
}

/**
  Zwolnienie zarezerwowanego przes funkcje *_setup
  @param[in] state lista dzieci drzewa Trie
*/
static int list_trie_teardown(void **state) {
    struct list_trie *l = *state;
    list_trie_done(l);
    free(l->child);
    free(l);
    return 0;
}

/**
  Podstawowa funkcja wywołująca wszystkie testy
*/
int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(trie_init_test),
        cmocka_unit_test_setup_teardown(trie_move_index_test, list_trie_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_move_index_on_delete_test, list_trie_delete_index_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_reallocate_child_test, list_trie_reallocate_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_reallocate_child2_test, list_trie_reallocate_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_search_for_place_test, list_trie_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_search_for_place2_test, list_trie_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_search_for_place3_test, list_trie_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(trie_search_for_place4_test, list_trie_add_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(find_word_test, list_trie_delete_word_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(insert_new_word_test, list_trie_add_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(insert_new_word2_test, list_trie_add_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(insert_new_word3_test, list_trie_add2_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(delete_recursion_test, list_trie_delete_word_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(delete_test, list_trie_delete_word_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(hints_remove_test, list_trie_delete_word_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(hints_add_test, list_trie_delete_word_setup, list_trie_teardown),
        cmocka_unit_test_setup_teardown(hints_change_test, list_trie_delete_word_setup, list_trie_teardown),

        //cmocka_unit_test_setup_teardown(word_list_repeat_test, word_list_setup, word_list_teardown),
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
