#include <gtk/gtk.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "editor.h"
#include "word_list.h"
#include "dictionary.h"
#include <wctype.h>

bool tag_exist = false;

void show_about () {
  GtkWidget *dialog = gtk_about_dialog_new();

  gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(dialog), "Text Editor");
  //gtk_window_set_title(GTK_WINDOW(dialog), "About Text Editor");
  
  gtk_about_dialog_set_comments(GTK_ABOUT_DIALOG(dialog), 
     "Text Editor for IPP exercises\n");
    
  gtk_dialog_run(GTK_DIALOG (dialog));
  gtk_widget_destroy(dialog);
}

void show_help (void) {
  GtkWidget *help_window;
  GtkWidget *label;
  char help[5000];

  help_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW (help_window), "Help - Text Editor");
  gtk_window_set_default_size(GTK_WINDOW(help_window), 300, 300);
 
  strcpy(help,
         "\nAby podłączyć usługę spell-checkera do programu trzeba:\n\n"
         "Dołączyć ją do menu 'Spell' w menubar.\n\n"
         "Pobrać zawartość bufora tekstu z edytora: całą lub fragment,\n"
         "  zapamiętując pozycję.\n\n");
  strcat(help, "\0");

  label = gtk_label_new(help);
    
  gtk_container_add(GTK_CONTAINER(help_window), label); 

  gtk_widget_show_all(help_window);
}

static void CheckDoc (GtkMenuItem *item, gpointer data) {
    GtkTextIter start, end, end2;
    gunichar *wword;

    if (!tag_exist)
    {
        gtk_text_buffer_create_tag(editor_buf, "red_fg",
                                 "foreground", "red",
                                 "weight", PANGO_WEIGHT_BOLD, NULL);
        tag_exist = true;
    }

      gtk_text_buffer_get_start_iter(editor_buf, &end);
      gtk_text_buffer_get_end_iter(editor_buf, &end2);
      for (;;) {
        char *word;

        gtk_text_iter_forward_word_end(&end);
        start = end;
        gtk_text_iter_backward_word_start(&start);
        word = gtk_text_iter_get_text(&start, &end);

        // Zamieniamy na wide char (no prawie)
        wword = g_utf8_to_ucs4_fast(word, -1, NULL);

        if (!dictionary_find(dict, (wchar_t *)wword))
            gtk_text_buffer_apply_tag_by_name(editor_buf, "red_fg",
                                            &start, &end);
        g_free(word);

        if (gtk_text_iter_compare(&end2, &end) == 0)
            break;
      }
}

//
static void WhatLoad (GtkMenuItem *item, gpointer data) {
    char *lang_list;
    GtkWidget *dialog;
    GtkWidget *vbox, *label, *combo;
    size_t lang_list_len = 0;
    size_t i = 0;
    if (dictionary_lang_list(&lang_list, &lang_list_len) == 0)
    {
        dialog = gtk_dialog_new_with_buttons("Slowniki", NULL, 0,
                                             GTK_STOCK_OK,
                                             GTK_RESPONSE_ACCEPT,
                                             GTK_STOCK_CANCEL,
                                             GTK_RESPONSE_REJECT,
                                             NULL);
        // W treści dialogu dwa elementy
        vbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
        // Tekst
        label = gtk_label_new("Lista dostepnych slownikow");
        gtk_widget_show(label);
        gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 1);

        // Spuszczane menu
        combo = gtk_combo_box_text_new();
        while(i < lang_list_len) {

          // Dodajemy kolejny element
          gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), lang_list+i);
          i += strlen(lang_list+i) + 1;
        }
        gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);
        gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 1);
        gtk_widget_show(combo);

        if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT)
        {
            if (dict != NULL)
            {
                if (dict_name != NULL)
                    dictionary_save_lang(dict, dict_name);
                dictionary_done(dict);
            }
            if (dict_name != NULL)
                free(dict_name);

            dict_name =
              gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(combo));
            dict = dictionary_load_lang(dict_name);
            if (dict == NULL)
            {
                dict = dictionary_new();
                dict_name = NULL;
            }

        }
        gtk_widget_destroy(dialog);
        free(lang_list);
    }

}
// Procedurka obsługi

int make_lowercase(wchar_t *word)
{
    for (wchar_t *w = word; *w; ++w)
        if (!iswalpha(*w))
            return 0;
        else
            *w = towlower(*w);
    return 1;
}

static void WhatCheck (GtkMenuItem *item, gpointer data) {
  GtkWidget *dialog, *dialog2, *dialog3;
  GtkTextIter start, end;
  char *word;
  gunichar *wword;
  
  // Znajdujemy pozycję kursora
  gtk_text_buffer_get_iter_at_mark(editor_buf, &start,
                                   gtk_text_buffer_get_insert(editor_buf));

  // Jeśli nie wewnątrz słowa, kończymy
  if (!gtk_text_iter_inside_word(&start)) {
    dialog = gtk_message_dialog_new(NULL, 0, GTK_MESSAGE_ERROR,
                                    GTK_BUTTONS_OK,
                                    "Kursor musi być w środku słowa");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
    return;
  }

  // Znajdujemy początek i koniec słowa, a potem samo słowo
  end = start;
  gtk_text_iter_backward_word_start(&start);
  gtk_text_iter_forward_word_end(&end);
  word = gtk_text_iter_get_text(&start, &end);

  // Zamieniamy na wide char (no prawie)
  wword = g_utf8_to_ucs4_fast(word, -1, NULL);

  // Sprawdzamy
  if (dictionary_find(dict, (wchar_t *)wword)) {
    dialog = gtk_message_dialog_new(NULL, 0, GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
                                    "Wszystko w porządku,\nśpij spokojnie");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
  }
  else {
    // Czas korekty
    GtkWidget *vbox, *label, *combo;
    struct word_list hints;
    int i;
    const wchar_t *words;

    dictionary_hints(dict, (wchar_t *)wword, &hints);
    words = word_list_get(&hints);
    dialog = gtk_dialog_new_with_buttons("Korekta", NULL, 0,
                                         GTK_STOCK_OK,
                                         GTK_RESPONSE_ACCEPT,
                                         GTK_STOCK_CANCEL,
                                         GTK_RESPONSE_REJECT,
                                         NULL);
    // W treści dialogu dwa elementy
    vbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));
    // Tekst
    label = gtk_label_new("Coś nie tak, mam kilka propozycji");
    gtk_widget_show(label);
    gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 1);

    // Spuszczane menu
    combo = gtk_combo_box_text_new();
    for (i = 0; i < word_list_size(&hints); i++) {
      // Combo box lubi mieć Gtk
      char *uword = g_ucs4_to_utf8((gunichar *)words[i], -1, NULL, NULL, NULL);

      // Dodajemy kolejny element
      gtk_combo_box_text_append_text(GTK_COMBO_BOX_TEXT(combo), uword);
      g_free(uword);
    }
    gtk_combo_box_set_active(GTK_COMBO_BOX(combo), 0);
    gtk_box_pack_start(GTK_BOX(vbox), combo, FALSE, FALSE, 1);
    gtk_widget_show(combo);

    if (gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_ACCEPT) {
      char *korekta =
        gtk_combo_box_text_get_active_text(GTK_COMBO_BOX_TEXT(combo));

      // Usuwamy stare
      gtk_text_buffer_delete(editor_buf, &start, &end);
      // Wstawiamy nowe
      gtk_text_buffer_insert(editor_buf, &start, korekta, -1);
      g_free(korekta);
    }
    else
    {
        dialog2 = gtk_dialog_new_with_buttons("Update Dictionary?", NULL, 0,
                                              GTK_STOCK_YES,
                                              GTK_RESPONSE_ACCEPT,
                                              GTK_STOCK_NO,
                                              GTK_RESPONSE_REJECT,
                                              NULL);

        vbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog2));
        // Tekst

        label = gtk_label_new("Czy chcesz dodać sprawdzane słowo do słownika?");
        gtk_widget_show(label);
        gtk_box_pack_start(GTK_BOX(vbox), label, FALSE, FALSE, 1);

        if (gtk_dialog_run(GTK_DIALOG(dialog2)) == GTK_RESPONSE_ACCEPT)
        {
            if (!make_lowercase((wchar_t *)wword))
            {

                dialog3 = gtk_message_dialog_new(NULL, 0, GTK_MESSAGE_INFO, GTK_BUTTONS_OK,
                                                "Nie można dodać słowa do słownika,\n zawiera niedopuszczalne znaki");
                gtk_dialog_run(GTK_DIALOG(dialog3));
                gtk_widget_destroy(dialog3);
            }
            else
            {
            dictionary_insert(dict, (wchar_t *)wword);
            gtk_text_buffer_remove_all_tags(editor_buf,
                                            &start, &end);
            }
        }
        gtk_widget_destroy(dialog2);
    }
    gtk_widget_destroy(dialog);
    word_list_done(&hints);
  }
  g_free(word);
  g_free(wword);
}

// Tutaj dodacie nowe pozycje menu

void extend_menu (GtkWidget *menubar) {
  GtkWidget *spell_menu_item, *spell_menu, *check_item, *load_dictionary, *check_document;

  spell_menu_item = gtk_menu_item_new_with_label("Spell");
  spell_menu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(spell_menu_item), spell_menu);
  gtk_widget_show(spell_menu_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menubar), spell_menu_item);

  check_item = gtk_menu_item_new_with_label("Check Word");
  g_signal_connect(G_OBJECT(check_item), "activate", 
                   G_CALLBACK(WhatCheck), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(spell_menu), check_item);
  gtk_widget_show(check_item);

  load_dictionary = gtk_menu_item_new_with_label("Load Dict");
  g_signal_connect(G_OBJECT(load_dictionary), "activate",
                   G_CALLBACK(WhatLoad), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(spell_menu), load_dictionary);
  gtk_widget_show(load_dictionary);

  check_document = gtk_menu_item_new_with_label("Check Document");
  g_signal_connect(G_OBJECT(check_document), "activate",
                   G_CALLBACK(CheckDoc), NULL);
  gtk_menu_shell_append(GTK_MENU_SHELL(spell_menu), check_document);
  gtk_widget_show(check_document);
}

/*EOF*/
