/** @defgroup dict-editor Moduł dict-check
    Program sprawdzający dany tekst
  */
/** @file
    Główny plik modułu dict-check
    @ingroup dict-editor
    @author Roland Maksimowicz <mak.rolandas@gmail.com>
    @date 2015-05-11
    @copyright Uniwersytet Warszawski
  */

#include "dictionary.h"
#include <string.h>
#include <wctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <locale.h>

///global var for argument -v
int parameterOn;

/**
    Maksymalna długość słowa bez kończącego znaku '\0'
  */
#define MAX_WORD_LENGTH 63

/** Porównuje dwa słowa między sobą
  @param[in] first Słowo nr 1.
  @param[in] second Słowo nr 2.
  @param[in] firstLen Długość słowa nr 1.
  @param[in] secondLen Długość słowa nr 2.
  @return true, jeśli słowa są takie same, false w p.p.
 */
bool compareTwoString(char *first, char *second, int firstLen, int secondLen) {
    int i;
    if (firstLen != secondLen)
        return false;
    for (i = 0; i < firstLen; i++) {
        if (*(first + i) != *(second + i))
            return false;
    }
    return true;
}

/** Wypisuje możliwe podpowiedzi na stderr.
  @param[in] dict Słownik
  @param[in] word Słowo, dla którego są wyszukiwane podpowiedzi.
 */
void hints_stderr(const struct dictionary *dict, wchar_t *word)
{
    struct word_list list;
    dictionary_hints(dict, word, &list);
    const wchar_t * const *a = word_list_get(&list);
    for (size_t i = 0; i < word_list_size(&list); ++i) {
        fprintf(stderr, " %ls", a[i]);
    }
    fprintf(stderr, "\n");
    word_list_done(&list);
}

/** Wypisuje dane słowo na standardowe wyjście,
 * Jeśli nieegzystuje słowo w słowniku wypisywany jest znak # przed danym słowem
  @param[in] dict Słownik.
  @param[in] line Słowo, które jest wypisywane na standardowe wyjście
  @param[in] lineNumber Numer wiersza
  @param[in] charNumber Numer znaku
 */
void print_to_std(const struct dictionary *dict, wchar_t line[], size_t lineNumber, size_t charNumber)
{
    wchar_t lineLower[MAX_WORD_LENGTH+1];
    size_t i = 0;

    size_t len = wcslen(line);

    for (i = 0; i < len; ++i)
        lineLower[i] = towlower(line[i]);

    lineLower[len] = line[len];

    if(dictionary_find(dict, lineLower))
    {
        printf("%ls", line);
    }
    else
    {
        printf("#%ls", line);
        if (parameterOn == 1)
        {
            fprintf(stderr, "%zu,%zu ", lineNumber, charNumber);
            fprintf(stderr, "%ls:", line);
            hints_stderr(dict, lineLower);
        }
    }
}

/** Wczytuje znaki z pliku, oraz przepisuje znaki, na standardowe wejście dla których funkcja iswalpha() zwraca false
  @param[in] dict Słownik.
 */
void read_from_std(const struct dictionary *dict)
{
    wchar_t line[MAX_WORD_LENGTH+1];
    wchar_t c;
    size_t lineNumber = 1;
    size_t charNumber = 1;
    size_t charCountTemp = 1;
    size_t counter = 0;
    while ((c = getwchar()) != EOF) {
        if (!iswalpha(c)) {
            line[counter] =L'\0';
            print_to_std(dict, line, lineNumber, charNumber);
            counter = 0;
            while (!iswalpha(c)) {
                printf("%c", c);
                if (c == '\n') {
                    charCountTemp = 0;
                    lineNumber++;
                }
                if ((c = getwchar()) == EOF)
                    break;
                charCountTemp++;

            }
            charNumber = charCountTemp;
        }
        line[counter] = c;
        counter++;
        charCountTemp++;
    }
    if (counter > 0) {
        line[counter] = L'\0';
        print_to_std(dict, line, lineNumber, charNumber);
    }
}

/** Wczytuje słownik z pliku
  @param[in] nazwa pliku
  @return zwraca słownik jeśli powiodłow się wczytanie z pliku, NULL w p.p.
 */
struct dictionary * load_dictionary(char* file)
{
    FILE *f = fopen(file, "r");
    struct dictionary *new_dict;
    if (!f || !(new_dict = dictionary_load(f))) {
        fprintf(stderr, "Failed to load dictionary\n");
        return NULL;
    }
    fclose(f);
    printf("dictionary loaded from file %s\n", file);
    return new_dict;
}

/** Funkcja main.
  Główna funkcja do sprawdzania tekstu.
  @param[in] ilość parametrów
  @param[in] tablica parametrów
  @return zwraca 1 jeśli słownik nie został wczytany, 0 w p.p.
 */
int main(int argc, char *argv[])
{
    setlocale(LC_ALL, "pl_PL.UTF-8");

    parameterOn = 0;
    if (argc == 3) {
        if (argv[1] != NULL) {
            if (compareTwoString(argv[1], "-v", strlen(argv[1]), 2))
                parameterOn = 1;
        argv[1]=argv[2];
        }
    }
    if (argc < 2)
    {
        fprintf(stderr, "Failed to load dictionary\n");
        return 1;
    }
    struct dictionary *new_dict;
    if ((new_dict = load_dictionary(argv[1])) == NULL)
        return 1;

    read_from_std(new_dict);
    dictionary_done(new_dict);
    return 0;
}
